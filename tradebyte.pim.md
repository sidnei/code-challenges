Hi Sidnei,

I have prepared the repository for you and have the task description here!
I will also add you to a hangouts chat with all developers where you can chat with them and are able to ask if any questions arise!

The task is the following:

Subject of this challenge is to create a minimalist Product Information Management (PIM) system as a web application.

The frontend part should be a modern single page application exchanging data with its backend via REST API.
The backend should be a well crafted application, persisting its data in a database.
Please pay attention to clean code and document your work properly.
Automated tests are desirable.
The choice of technologies you use is completely up to you but be prepared to explain the reasons behind your choices.
Input should be validated as needed and also adhere to common security standards.
Think about scalability and performance
The application should provide the following two data structures and appropriate CRUD operations to manage them:

Article

SKU (article number)
EAN (European article number)
Name
Stock quantity
Price
Category

Name
Features

Categories may be nested into each other forming a tree structure.
Multiple categories may be assigned to one article.
The same category can be assigned to an unlimited number of articles, but only once to each article.
It should be possible to mass-assign multiple categories to a single article in one step.
Deleting a category is possible at all times, yet it doesn't delete any of the articles.
Additionally it should be possible to sort and filter by appropriate attributes.


Please push all your work to the git repository given to you.



Have fun!
