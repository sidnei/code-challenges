## Qual é o teste?

Deverá ser desenvolvido um repositório de arquivos para uso na intranet de um
cliente pelos funcionários.


## O que deve ser feito ?

O sistema consiste em 2 tipos de usuários gestor e funcionário com as seguintes
funcionalidades:

Gestor:

- Visualizar arquivos de todos os usuários.
- Controlar usuários com informações básicas como endereço e contato
- Visualizar números de arquivos e volume gasto total e por usuário
- Colocar limites de upload por volume de disco ou número de arquivos, ou
- deixar livre para usar a vontade

Funcionário:

- Visualizar disco disponível e já usado
- Upload, download, delete de arquivos
- Editar dados pessoais menos email e nome
- Marcar arquivos como públicos
- Terá um área externa dos usuário onde será possível ver todos os arquivos públicos.
- API que retorna números de usuários, disco usado e números de arquivos.


## O que devo utilizar ?

- Python 3.x
- Framework Django
- Mysql
- Materialize google
- Estará livre para utilizar outras ferramentas complementares.
- Para versionamento do projeto utilize o GIT e hospede no GITHUB.
- Na raiz deverá ter um arquivo readme com explicações de alguns pontos chaves do código, com motivo de ser feito daquele jeito.
- O sistema deverá ser disponibilizado em algum servidor como AWS, Digital Ocean, Azure ou outro de sua preferência.
- Após finalizar enviar por email o link do GITHUB do projeto e do servidor.
- Entrega mesmo se não tiver concluído todos requisitos.


    "Os pequenos detalhes são sempre os mais importantes." Sherlock Holmes
